const input = document.querySelector('.item__input');
const buttonDown = document.querySelector('.button_down');
const buttonStop = document.querySelector('.button_stop');
const buttonClear = document.querySelector('.button_clear');
const flipHourTens = document.querySelector('[hours-tens]')
const flipHourOnes = document.querySelector('[hours-ones]')
const flipMinuteTens = document.querySelector('[minutes-tens]')
const flipMinuteOnes = document.querySelector('[minutes-ones]')
const flipSecondTens = document.querySelector('[seconds-tens]')
const flipSecondOnes = document.querySelector('[seconds-ones]')

let timerId;
let totalSecond = 0;
input.addEventListener('input', inputValue);
buttonDown.addEventListener('click', controlButtons);
buttonStop.addEventListener('click', controlButtons);
buttonClear.addEventListener('click', controlButtons);

function inputValue(event) {
	totalSecond = event.target.value;

	if (isNaN(totalSecond)) {
		return
	} else {
		createTimerAnimator(+totalSecond)
	}
}

function controlButtons() {
	if (this.className === "button_down") {
		getSecondsDown();
	}

	if (this.className === "button_stop") {
		clearInterval(timerId);
	}

	if (this.className === "button_clear") {
		input.value = '';
		createTimerAnimator(0);
		clearInterval(timerId)
	}
};
const getSecondsDown = () => {
	clearInterval(timerId)
	timerId = setInterval(() => {
		totalSecond--;
		createTimerAnimator(totalSecond);
	}, 1000)
}

const createTimerAnimator = (totalSecond) => {
	const seconds = totalSecond % 60
	const minutes = Math.floor(totalSecond / 60) % 60
	const hours = Math.floor(totalSecond / 3600)

	flip(flipHourTens, Math.floor(hours / 10))
	flip(flipHourOnes, hours % 10)
	flip(flipMinuteTens, Math.floor(minutes / 10))
	flip(flipMinuteOnes, minutes % 10)
	flip(flipSecondOnes, seconds % 10);
	flip(flipSecondTens, Math.floor(seconds / 10));
};

function flip(flipCard, newNumber) {

	const topHalf = flipCard.querySelector(".top");
	const bottomHalf = flipCard.querySelector('.bottom');
	const topFlip = document.createElement("div");
	topFlip.classList.add('top-flip');
	const bottomFlip = document.createElement('div');
	bottomFlip.classList.add('bottom-flip');

	top.textContent = newNumber;
	bottomHalf.textContent = newNumber;
	topFlip.textContent = newNumber;
	bottomFlip.textContent = newNumber;

	topFlip.addEventListener('animationstart', e => {
		topHalf.textContent = newNumber;
	})

	topFlip.addEventListener('animationend', e => {
		topFlip.remove();
	})

	bottomFlip.addEventListener('animationend', e => {
		bottomHalf.textContent = newNumber;
		bottomFlip.remove()
	})

	flipCard.append(topFlip, bottomFlip);
}